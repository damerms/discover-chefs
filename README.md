### What is this repository for? ###

Discover Chefs Portfolio Website
*Final version

2008 - 2015: Conceptualized, designed marked up HTML and CSS for a chef portfolio web application with a programmer to understand designing with MVC (CakePHP) frameworks and templates.

Results: 100s of chef sign ups. E-newsletter. 1000s of social media followers.