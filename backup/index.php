<?php

if ($_POST["email"] != "" )
{
  $email = $_POST["email"];
  mail("merms@rcn.com","Discover Chefs: Pre-Launch Sign Up - ".$email,"Sign-up from: ".$email);
  mail("ian@olsen-clark.com","Discover Chefs: Pre-Launch Sign Up - ".$email,"Sign-up from: ".$email);
  header("location: http://www.discoverchefs.com/thanks.php");
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:xf="urn:nterface.com:xf:1.0" xml:lang="en">
<head>
	<title>Chef Portfolios/Chef Profiles/Chef Showcase & Promotional Website</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="Need a chef portfolio/chef profile - Promote your culinary skills or locate a culinary     professional on DiscoverChefs.com" />
	<link href="/assets/css/discoverchefs.css" rel="stylesheet" type="text/css" media="screen" />
	<link href="/assets/css/error.css" rel="stylesheet" type="text/css" media="screen" />
	<link rel="icon" href="/favicon.ico" type="image/x-icon" />

<body>

<!-- Start Header -->
<div class="aquabar"></div>
<div id="container">
	<div id="header">
		<div id="logo"><img src="/assets/images/logos/discover_chefs_logo.gif" alt="Discover Chefs" width="353" height="85" /></div>
  	</div>

	<div id="maincontent">
	<h1>What is Discover Chefs? </h1>

	<p><a href="http://www.eatupdrinkup.com" target="_blank">Eatup Drinkup</a> Inc. creators of <a href="http://www.foodanddrinkjobs.com" target= "_blank"> FoodandDrinkjobs.com</a> brings you Discover Chefs. Discover Chefs provides culinary professionals a simple web-based tool to host their profile as well as showcase professional & personal work.</p>
         
	<h1>Why Join Discover Chefs?</h1>
          
      	 <ul id="navlist">
		   <li><span class="highlight">Accentuate  your resume on  job sites </span></li>
		   <br />
		   <li><span class="highlight">Extend your profile from  networking sites</span></li>	
		   <br />
		   <li><span class="highlight">Compliment or replace your personal website</span></li>
		   <br />
	       <li><span class="highlight">Hire future employees</span></li>
		   <br />
		   <li><span class="highlight">Share ideas or ask questions with simple messaging </span></li>
		   <br />   
		   <li><span class="highlight">Find a new partner for a  resturant concept</span></li>
		   <br />
		   <li><span class="highlight">Promote culinary services, recipes, menus, etc... </span></li>
		 </ul> 
		  
 	<h1>Enter your email and we will let you know when we are ready for launch! </h1>
            
		  <form id="form1" method="post" action="">
            <input name="email" type="text" id="email" accesskey="1" tabindex="1" size="40" />
            <label>
            <input type="submit" name="Submit" value="Send" />
            </label>
          </form>
	</div> 

<div id="footer">
		<div id="footermenu"> 
		Check out our <a href="http://discoverchefs.wordpress.com" target="_self">blog</a> to keep up on what's going on!
		</div>
		<div id="copyright">
		&copy; <a href="http://www.eatupdrinkup.com">Eatup Drinkup,Inc.</a>
		</div>
 	</div>
 </div>
		
<script type="text/javascript" src="http://include.reinvigorate.net/re_.js"></script>
<script type="text/javascript">
re_("555y7-769hoh6v8c");
</script>

</body>
</html>
