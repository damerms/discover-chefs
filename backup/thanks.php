<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:xf="urn:nterface.com:xf:1.0" xml:lang="en">
<head>
	<title>Discover Chefs.com</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link href="/assets/css/discoverchefs.css" rel="stylesheet" type="text/css" media="screen" />

<body>

<!-- Start Header -->
<div class="aquabar"></div>
<div id="container">
	<div id="header">
		<div id="logo"><img src="/assets/images/logos/discover_chefs_logo.gif" alt="Discover Chefs" width="353" height="85" /></div>
  	</div>

	<div id="maincontent">
	<h1>Thanks! We will email with launch information when we're ready! </h1>

	<h1>&nbsp;</h1>
          
   	  <label></label>
  </div>
	<div id="footer">
		<div id="footermenu"> 
		Check out our <a href="http://discoverchefs.wordpress.com" target="_self">blog</a> to keep up on what's going on!
		</div>
		<div id="copyright">
		&copy; <a href="http://www.eatupdrinkup.com">Eatup Drinkup,Inc.</a>
		</div>
 	</div>
 </div>
		
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-556926-1";
urchinTracker();
</script>

<script type="text/javascript" src="http://include.reinvigorate.net/re_.js"></script>
<script type="text/javascript">
re_("555y7-769hoh6v8c");
</script>

</body>
</html>
