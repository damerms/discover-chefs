<?php

class ChefsHelper extends Helper
{

  var $helpers = array("Html");

  /*

    UI Helpers

  */


  //  Deprecated ???

  function loginStatus($login = false)
  {
    if ($login->check($id))
      $result = "<a href='~/users/logout'>Logout</a>";
    else
      $result = "<a href='/users/login'>Login</a>";
    return $this->output($result);
  }

  //

  function login()
  {
    return $this->output("<a href='/users/login'>Login</a>");

  }

  //
  //
  //

  function logout()
  {
    return "";
  }

  //
  //  UI Helpers
  //


	
  function alertPanel($message="")
  {
    $html = "";

    if (!empty($message))
    {
        $html = '<div class="messageExplanation" id="messageExplanation">';
        $html .= '<p class="messageExplanation">'.$message.'</p></div>';
    }

    return $this->output($html);

  }

    function errorPanel($title='',$subTitle='')
    {
      $html = '<div class="errorExplanation" id="errorExplanation">';
      $html .= '<h2>'.$title.'</h2>';
      $html .= '<p>'.$subTitle.'</p>';
      $html .= '</div>';
      
      return $this->output($html);

    }

  function getAppPath()
  {
    //return "/discoverchefs/";
    return "/test/";
  }

  function getImagePath()
  {
    //return FILES;
    return $this->getApppath()."files/";
    //return ."files/";
  }

  function htmlFormat($html)
  {
    $html = htmlentities($html);
    $html = ereg_replace("\r\n", '<br/>', $html);
    
    return $html;
  }

  function popup($item="")
  {
    $title = "(Tell Me More..)";
    $url = "/pages/popup/$item";
    $result = $this->Html->link("$title","$url",array("class"=>"popup"));
    return $this->output($result);
  }

  function summaryLine($data)
  {
    return $data["Profile"]["firstname"]." ".$data["Profile"]["lastname"]." - ".$data["Profile"]["title"]." - ".$data["Profile"]["city"].", ".$data["Profile"]["state"];
  }

  function urlFormat($url)
  {
    if (substr($url,0,7) != "http://")
      return "http://".$url;
    else
      return $url;
  }

  /*
    Data Helpers
  */

  function get_full_state_list()
  {
    $state_list = array(
    ''=>"Please Select A State",
    'AL'=>"Alabama",
    'AK'=>"Alaska",
    'AZ'=>"Arizona",
    'AR'=>"Arkansas",
    'CA'=>"California",
    'CO'=>"Colorado",
    'CT'=>"Connecticut",
    'DE'=>"Delaware",
    'DC'=>"District Of Columbia",
    'FL'=>"Florida",
    'GA'=>"Georgia",
    'HI'=>"Hawaii",
    'ID'=>"Idaho",
    'IL'=>"Illinois",
    'IN'=>"Indiana",
    'IA'=>"Iowa",
    'KS'=>"Kansas",
    'KY'=>"Kentucky",
    'LA'=>"Louisiana",
    'ME'=>"Maine",
    'MD'=>"Maryland",
    'MA'=>"Massachusetts",
    'MI'=>"Michigan",
    'MN'=>"Minnesota",
    'MS'=>"Mississippi",
    'MO'=>"Missouri",
    'MT'=>"Montana",
    'NE'=>"Nebraska",
    'NV'=>"Nevada",
    'NH'=>"New Hampshire",
    'NJ'=>"New Jersey",
    'NM'=>"New Mexico",
    'NY'=>"New York",
    'NC'=>"North Carolina",
    'ND'=>"North Dakota",
    'OH'=>"Ohio",  
    'OK'=>"Oklahoma",
    'OR'=>"Oregon",
    'PA'=>"Pennsylvania",  
    'RI'=>"Rhode Island",  
    'SC'=>"South Carolina",  
    'SD'=>"South Dakota",
    'TN'=>"Tennessee",  
    'TX'=>"Texas",
    'UT'=>"Utah",  
    'VT'=>"Vermont",  
    'VA'=>"Virginia",  
    'WA'=>"Washington",  
    'WV'=>"West Virginia",
    'WI'=>"Wisconsin",  
    'WY'=>"Wyoming");
    
    return $this->output($state_list);
  }  
  
  function lookup_state($state_code)
  {
    $states = $this->get_full_state_list();
    return $this->output($states[$state_code]);
  }

    //    Private Helpers

    function _validateErrors()
    {
	    $objects = func_get_args();
		if (!count($objects))
        {
			return false;
		}

		$errors = array();
		foreach($objects as $object)
        {
			$errors = array_merge($errors, $object->invalidFields($object->data));
		}
		return $this->validationErrors = (count($errors) ? $errors : false);
	}

}

?>
