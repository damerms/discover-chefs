<?php

class TextAdsHelper extends Helper
{

  var $helpers = array("Html");

  //
  //  UI Helpers
  //

  function displayTextAds()
  {
    $html = 'This is a banner ad';
    
    $ad[0] = "Eatup Drinkup Inc. Brings You Discover Chefs!";
    $url[0] = "http://www.eatupdrinkup.com"; 
    
    $ad[1] = "Food & Drink Jobs";
    $url[1] = "http://www.foodanddrinkjobs.com"; 
    
    $random_ad = rand(0,sizeof($ad)-1);
    
    $html = "<a href='".$url[$random_ad]."' target='_blank'>".$ad[$random_ad]."</a>";
    
    return $this->output($html);
  }

	
  function alertPanel($message="")
  {
    $html = "";

    if (!empty($message))
    {
        $html = '<div class="messageExplanation" id="messageExplanation">';
        $html .= '<p class="messageExplanation">'.$message.'</p></div>';
    }

    return $this->output($html);

  }

 
}

?>
