    <h1><?php echo $chefs->htmlFormat($data["Profile"]["firstname"]." ".$data["Profile"]["lastname"])." - ".$data["Profile"]["title"]." - ".$data["Profile"]["city"].", ".$data["Profile"]["state"];  ?></h1>

    <table class="profile-table">
      <?php if (!empty($data["Profile"]["website"])) : ?>
      <tr>
        <th class="profile-label"><label for="chef_website" accesskey="w">Website</label></th>

        <td><?php echo $html->link($data["Profile"]["website"],$chefs->urlFormat($data["Profile"]["website"]),array("target"=>"_blank")); ?></td>
      </tr>
      <?php endif; ?>
      
      <tr>
        <th class="profile-label"><label for="chef_description" accesskey="d">About Me</label></th>

        <td class="profil-data"><?php echo $chefs->htmlFormat($data["Profile"]["description"]); ?></td>
      </tr>

    <?php if (!empty($data["Profile"]["affiliations"])) : ?>
    <tr>
      <th><label for="chef_description" accesskey="d">Affliiations</label></th>

      <td><?php echo $chefs->htmlFormat($data["Profile"]["affiliations"]); ?></td>
    </tr>
    <?php endif; ?>

    <?php if (!empty($data["Profile"]["achievements"])) : ?>
    <tr>
      <th><label for="chef_description" accesskey="d">Achievements</label></th>

      <td><?php echo $chefs->htmlFormat($data["Profile"]["achievements"]); ?></td>
    </tr>
    <?php endif; ?>

      <tr>
        <th class="profile-label"><label for="chef_experience" accesskey="x">Experience</label></th>

        <td class="profile-data"><?php echo $chefs->htmlFormat($experience[$data["Profile"]["experience"]]); ?></td>
      </tr>

      <tr>
        <th class="profile-label"><label for="chef_availability" accesskey="a">Availability</label></th>

        <td class="profile-data"><?php echo $chefs->htmlFormat($availability[$data["Profile"]["availability"]]); ?></td>
      </tr>

      <tr>
        <th class="profile-label"><label for="chef_specialites" accesskey="p">Specialites</label></th>

        <td class="profile-label"><?php echo $chefs->htmlFormat($data["Profile"]["specialities"]); ?></td>
      </tr>

      <tr>
        <th class="profile-label"><label for="chef_cuisines" accesskey="u">Cuisines</label></th>

        <td class="profile-data"><?php echo $chefs->htmlFormat($data["Profile"]["cuisines"]); ?></td>
      </tr>
      <tr>
        <th class="profile-label"><label for="chef_influences" accesskey="i">Influences</label></th>

        <td class="profile-data"><?php echo $chefs->htmlFormat($data["Profile"]["influences"]); ?></td>
      </tr>
    </table>

    <?php if (!empty($images)) : ?>

    <h1>Gallery</h1>

    <table class="profile-table">
      <?php foreach($images as $image) :  ?>

      <tr>
        <th class="profile-label"><?php echo $image["ProfileImage"]["title"]; ?></th>
      </tr>

      <tr>
        <td class="profile-data">
        	<?php echo "<img width='400' height='300' border='1' src='".($chefs->getImagePath().$image["ProfileImage"]["filename"])."'/> - ";  ?></td>
      </tr><?php endforeach ?>
    </table>
    
    <?php endif; ?>
    
    <!-- End Chef Profile Form -->
