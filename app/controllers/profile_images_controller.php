<?php

//
//
//

class ProfileImagesController extends AppController
{

  var $name = "ProfileImages";

  var $helpers = array("form","html","chefs","textads");

  function beforeFilter()
  {
    parent::beforeFilter();
    $this->setSiteTitle();

    $this->checkAccess();
  }


  function delete($id)
  {
    $this->ProfileImage->del($id);
    $this->redirect("/profile_images/upload");
  }

  function upload()
  {

    
    if (!empty($this->data))
    {

      if (is_uploaded_file($this->data['ProfileImage']['imagefile']['tmp_name']))
      {

        $uploadfile = WWW_ROOT."files".DS.$this->data["ProfileImage"]["imagefile"]["name"];
        $data["ProfileImage"]["user_id"] = $this->Session->read("user_id");
        $data["ProfileImage"]["filename"] = $this->ProfileImage->getImageFileName($this->Session->read("user_id"));
        $data["ProfileImage"]["title"] = $this->params["data"]["ProfileImage"]["title"];
        $data["ProfileImage"]["profile_id"] = $this->ProfileImage->getUserProfileId($this->Session->read("user_id"));

        $extension = strtolower(substr($uploadfile, strrpos($uploadfile, '.') + 1));

        if ($extension == 'jpg')
        {
          $size = $this->data["ProfileImage"]["imagefile"]["size"];
          if ($size > 1000000)
          {
            $upload_error = 'The image file must be less than 1024K (1Mb) in size.';
          }
        }
        else
        {
          $upload_error = 'The file must be a JPG image';
        }  

            
        if (empty($upload_error) )
        {
          if ($this->ProfileImage->save($data))
            move_uploaded_file( $this->data["ProfileImage"]["imagefile"]["tmp_name"], WWW_ROOT."files".DS.$data["ProfileImage"]["filename"] );
        }
      }
      else
      {
        $upload_error = 'Please select an image file to upload';
      }

    }
    
    if (isset($upload_error))
      $this->set('upload_error',$upload_error);
    
    $id = $this->Session->read("user_id");
    $this->set("images",$this->ProfileImage->findAll("ProfileImage.user_id=$id"));
    
    $this->data['ProfileImage']['imagefile']['tmp_name'] = null;
    $this->data['ProfileImage']['imagefile']['name'] = null;
  }

}
?>
