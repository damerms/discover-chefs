<?php

class ProfilesController extends AppController
{

  var $name = "Profiles";

  var $helpers = array("form","html","chefs","textads");

  var $uses = array("Profile","ProfileImage","User");

  //
  //  Define Filters
  //
  

  function beforeFilter()
  {
    parent::beforeFilter();
    
    $this->setSiteTitle();
    
    if ($this->action != "states" && $this->action != "browse" && $this->action != "view" && $this->action != "cleanup" && $this->action != "printfriendly" && $this->action != "feedback" && $this->action != "member" && $this->action != "test")
    {
      $this->checkAccess();
    }
  }


  function browse()
  {
    $this->set("profiles",$this->Profile->getAllByState());
  }

  function cleanup()
  {
  	/*$this->Profile->query("DELETE  FROM profiles");	
  	$this->ProfileImage->query("DELETE  FROM profile_images");	
	  $this->User->query("DELETE FROM users"); */
  }
  
  function contactus()
  {
  }
  
  function create()
  {
    $this->set('error',false);
    
    if (!empty($this->data))
    {

	  $this->set("experience",$this->Profile->getExperienceLevels());
	  $this->set("availability",$this->Profile->getAvailabilityLevels());
		 
	  $this->data["Profile"]["user_id"] = $this->Session->read("user_id");
      if ($this->Profile->save($this->data))
      {
        $this->Session->write("profile_exists",true);
        $this->redirect("/profiles/myview");
      }
      else
      {
        $this->set('error',true);
      }
    }
	else
	{
		$this->set("experience",$this->Profile->getExperienceLevels());
    
    $this->set("availability",$this->Profile->getAvailabilityLevels());
	  $this->set("messaging_enabled",$this->Profile->getMessageOptions());
    $this->data["Profile"]["availability"] = "1";
	}

  }

  function edit($id=null)
  {

    $this->set('error',false);
    
    $this->set("experience",$this->Profile->getExperienceLevels());
	  $this->set("availability",$this->Profile->getAvailabilityLevels());
	  $this->set("messaging_enabled",$this->Profile->getMessageOptions());
    
    if (!empty($this->data))
    {
      if ($this->Profile->save($this->data))
      {
        $this->redirect("/profiles/myview");
        exit(0);
      }
      else
      {
        $this->set('error',true);
      }      
    }
    else
    {
      $this->data = $this->Profile->getProfileByUserId($id);  
    } 
    
  }

  function feedback()
  {
  }

  function home()
  {
  }
    
  function index()
  {
    if (!$this->Session->read("user_id"))
    {
      $this->redirect("/profile/create");
    }
  }

  function member($site_name)
  {
    //debug($this->params);
    //$site_name = $this->params['sitename'];
    
    $data = $this->User->getBySiteName($site_name); 
    $id = $data['User']['id'];
      if ($id != null)
      {
  		  $this->set("experience",$this->Profile->getExperienceLevels());
  		  $this->set("availability",$this->Profile->getAvailabilityLevels());
  
        $data = $this->Profile->getProfileByUserId($id);
  	    $this->set("data",$data);
  	    $this->set("images",$this->ProfileImage->getProfileImageByUserId($id));
  	    
  	    
    	  $this->pageTitle = $data["Profile"]["title"]." - ".$data["Profile"]["city"].", ".$data["Profile"]["state"]." - ".$data["Profile"]["firstname"]." ".$data["Profile"]["lastname"]." | Discover Chefs";
    	  
      }
      else
      {
        $this->redirect("http://discoverchefs.com");
      }
  }

  function myview()
  {
    if ($this->Session->read("profile_exists") == true)
    {
      $id = $this->Session->read("user_id");

	  $this->set("experience",$this->Profile->getExperienceLevels());
	  $this->set("availability",$this->Profile->getAvailabilityLevels());

      $this->set("data",$this->Profile->getProfileByUserId($id));
      $this->set("images",$this->ProfileImage->getProfileImageByUserId($id));
    }
    else
    {
      $this->redirect("/profiles/create");
    }
  }

  function preview()
  {
    if ($this->Session->read("profile_exists") == true)
    {
      $id = $this->Session->read("user_id");
      $this->set("data",$this->Profile->getProfileByUserId($id));
    }
    else
    {
      $this->redirect("/profiles/create");
    }
  }

  function printfriendly($id=null)
  {
      if ($id != null)
      {
      $this->layout = "plain";
      
		  $this->set("experience",$this->Profile->getExperienceLevels());
		  $this->set("availability",$this->Profile->getAvailabilityLevels());

	      $this->set("data",$this->Profile->getProfileByUserId($id));
	      $this->set("images",$this->ProfileImage->getProfileImageByUserId($id));
      }
      else
        $this->redirect("/");
  }
  
  
  
  function search()
  {
  }
  
	function states()
	{
    $this->set("profiles",$this->Profile->getAllByState());
	}

  function test($member=null)
  {
    $this->member($member);
  }

  function view($id=null)
  {
      if ($id != null)
      {
  		  $this->set("experience",$this->Profile->getExperienceLevels());
  		  $this->set("availability",$this->Profile->getAvailabilityLevels());
  
        $data = $this->Profile->getProfileByUserId($id);
  	    $this->set("data",$data);
  	    $this->set("images",$this->ProfileImage->getProfileImageByUserId($id));
  	    
  	    
    	  $this->pageTitle = $data["Profile"]["title"]." - ".$data["Profile"]["city"].", ".$data["Profile"]["state"]." - ".$data["Profile"]["firstname"]." ".$data["Profile"]["lastname"]." | Discover Chefs";
      }
      else
      {
        $this->redirect("/");
      }
  }

}
?>
