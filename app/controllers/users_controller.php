<?php

class UsersController extends AppController
{

  //
  //  Define Model Names
  //
  var $name = "Users";

  //
  //  Define Components
  //

  var $components = array('Email');

  //  Include Helper Classes

  var $helpers = array("Form","html","chefs","textads");

	
 	//  Define Filters

  function beforeRender()
  { 
    $this->setSiteTitle();
  }

 	// Check If Current User Authentcicated
	function beforeFilter()
	{

		if ($this->action != "index" && $this->action != "login" && $this->action != "signup" && $this->action != "signupcomplete" && $this->action != "tellafriend" )
		{
      		$this->checkAccess();
    	}
  	}

  	//  Define Public Methods

  function edit($id=null)
  {
    
  	$this->set('error',false);
      
  	if (!empty($this->data))
      {
        if ($this->User->save($this->data["User"]))
        {
          $this->redirect("/profiles/myview");
  	    }
        else
        {
          $this->set('error',true);
        }
      }
      else
      {
        $this->data = $this->User->read(null,$id);
        $this->data["User"]["confirm"] = $this->data["User"]["password"];
      }
    
  }


  //

  function home()
  {

  }


  //

  function index()
  {
		echo "Server Name: ".$_SERVER['HTTP_HOST']."<br/>";
  }


  //

  function login()
  {

    //$this->set('error',false);
    
    $this->set("email_error","");
    $this->set("password_error","");
    
    
    if (!empty($this->data["User"]))
    {

      $email_error = $this->User->authenticateUser($this->data["User"]["email"]);    
      $this->set('email_error',$email_error);
      
      if ( $email_error == "" )
      {
        $this->set('error',true);
        
        $this->set('data',$this->User->findByEmail($this->data["User"]["email"]));
        
        $password_error = $this->User->authenticatePassword($this->data["User"]["email"],$this->data["User"]["password"]);    
        $this->set('password_error',$password_error);        
        
        if ($password_error == "")
        {        
          $user = $this->User->findByEmail($this->data["User"]["email"]);
          $this->Session->write("user_id",$user["User"]["id"]);
  
          if ($this->User->checkProfileExists($this->Session->read("user_id")))
           $this->Session->write("profile_exists",true);
          else
            $this->Session->write("profile_exists",false);
  
          $this->redirect("/profiles/myview");
        }
        
      }
    }
  }


  //
  //
  //
  function logout()
  {
    $this->Session->destroy();
    $this->redirect("/");
  }

  function signup()
  {
     
  	$this->set('error',false);
    if (!empty($this->data["User"]))
    {
   		$this->User->set($this->data);
    	if ($this->User->validates() && $this->User->save($this->data["User"]))
		{
        	$this->Session->write("password",$this->data["User"]["password"]);
	        $this->Session->write("email",$this->data["User"]["email"]);
    	    $this->redirect("/users/sendemail");
      	}
		else
		{
			$this->set("error",true);
		}
    }
  }

  function signupcomplete()
  {
  }

  function sendemail()
  {
  	
    $this->set("email",$this->Session->read("email"));
    $this->set("password",$this->Session->read("password"));

    $this->Email->template = 'signup'; //name of thtml to include
    $this->Email->to = $this->Session->read("email");
    $this->Email->from = 'getdiscovered@discoverchefs.com';
    $this->Email->repyTo = 'getdiscovered@discoverchefs.com';
    $this->Email->sendAs = 'html';
    $this->Email->subject = 'Discover Chefs Sign-Up';
          
	  if ($this->Email->send())
      {
        $this->redirect("/users/tellafriend");
      }
	      
  }

  function tellafriend()
  {
    $this->redirect("/users/signupcomplete");
  }

  /*
  */
  function view()
  {
  }

  //  Define Private Methods
}

?>
