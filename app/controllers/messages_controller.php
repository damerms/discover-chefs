<?php

class MessagesController extends AppController
{
  //
  //  Define Model Names
  //
  var $name = "Messages";

  //
  //  Define Components
  //

  var $components = array('Email');

  //  Include Helper Classes

  var $helpers = array("Form","html","chefs","textads");

  //  Other Models
  
  var $uses = array("Message","Profile","ProfileImage","User");

	
 	//  Define Filters


 	// Check If Current User Authentcicated

	function beforeFilter()
	{
    $this->setSiteTitle();

		if ($this->action != "index" && $this->action != "login" && $this->action != "signup" && $this->action != "signupcomplete" && $this->action != "tellafriend" && $this->action != "create" && $this->action != "sent")
		{
      		$this->checkAccess();
    	}
  	}

  	//  Define Public Methods
  	
  	function create($id=null)
  	{
  	     	$this->User->id = $id;
  	      $this->set("user",$this->User->read());

  	   if (!empty($this->data))
  	   {
  	     if ($this->Message->save($this->data))
  	     {  	       
  	       if ($this->_sendemail($this->data["Message"]["from"],$this->data["Message"]["to"],$this->data["Message"]["subject"],$this->data["Message"]["body"]))
  	         $this->redirect("/messages/sent");
         }        
       }
       
    }
    
    
    function sent()
    {
    }
     
    //  Private Methods
    
  function _sendemail($from,$to,$subject,$body)
  {
  	
  	$this->set("body",$body);
  	
    $this->Email->template = 'message'; //name of thtml to include
    $this->Email->to = $to;
    $this->Email->from = $from;
    $this->Email->repyTo = $from;
    $this->Email->sendAs = 'html';
    $this->Email->subject = $subject;
    
	  if ($this->Email->send())
    {
      return true;	  
    }
    else
    {
      return false;
    }
  }

}

?>
