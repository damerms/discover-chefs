<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:xf="urn:nterface.com:xf:1.0" xml:lang="en">
<head>
	<title>Discover Chefs - Professional Profiles For Chefs</title>
	<meta name="Description" content="Discover Chef's provides culinary professionals a simple tool to host their profile and showcase professional &amp; 			     personal work." />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<?php echo $html->css("discoverchefs");  ?>
</head>
<body>
<div id="container">
  <!-- Start Main Content -->
  <div id="formcontent">
    <?php echo $content_for_layout; ?>
  </div>
  <!-- End Main Content -->
    </div>
  </body>
</html>
