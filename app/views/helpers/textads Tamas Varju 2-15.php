<?php

class TextAdsHelper extends Helper
{

  var $helpers = array("Html");

  //
  //  UI Helpers
  //

  function displayTextAds()
  {
    $html = 'This is a banner ad';

	  $title[0] = "January Chef of the Month:";
    $title_class[0] = "textad-lead";

    $ad[0] = "Tamas Varju - Chef - London, AL";
		$ad_class[0] = "textad-link";
    $url[0] = "http://www.discoverchefs.com/profiles/view/532";



	$random_ad = rand(0,sizeof($ad)-1);

    $html = "<span class='".$title_class[$random_ad]."'>".$title[$random_ad]."</span>"."<a class='".$ad_class[$random_ad]."'  href='".$url[$random_ad]."' target='_blank'>".$ad[$random_ad]."</a>";

    return $this->output($html);
  }


  function alertPanel($message="")
  {
    $html = "";

    if (!empty($message))
    {
        $html = '<div class="messageExplanation" id="messageExplanation">';
        $html .= '<p class="messageExplanation">'.$message.'</p></div>';
    }

    return $this->output($html);

  }


}

?>
