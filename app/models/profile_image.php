<?php

class ProfileImage extends Model
{

  var $name = "ProfileImage";

  //  Validation
  
	var $validate = array
		(
				'title' => VALID_NOT_EMPTY,
				'filename' => VALID_NOT_EMPTY
		); 

  //  Filters
  
  function beforeFilter()
  {
    parent::beforeFilter();
  }

  //
  //  Delete Image File
  //
    
  function beforeDelete()
  {
    
    $data = $this->read(null,$this->id);
    $filepath = WWW_ROOT."files".DS.$data["ProfileImage"]["filename"];
    if (unlink($filepath))
    {
      //echo 'File TRUE - '.$filepath;
      return true;
    }    
    else
    {
      //echo 'File FALSE - '.$filepath;
      return false;
    }
      
  }
  
  function getImageFileName($userid)
  {
      $sql = "SELECT MAX(id)+1 AS max FROM profile_images";
      $result = $this->query($sql);
      $max = $result[0][0]["max"];
      if (!$max)
      $max = 1;

      return "image".$userid."_".$max.".jpg";
  }


  function getAllByUser($userid)
  {
      return $this->findAll();
  }

  function getUserProfileId($userid)
  {
    $sql = "SELECT id  FROM profiles WHERE user_id = $userid";
    $result = $this->query($sql);
    $id = $result[0]["profiles"]["id"];
    return $id;
  }

  function getProfileImageByUserId($id=null)
  {
    $sql = "SELECT * FROM profile_images AS ProfileImage WHERE user_id = $id ORDER BY id DESC";
    $result = $this->query($sql);
    return $result;
  }

  function getProfileImageById($id=null)
  {
    $sql = "SELECT * FROM profile_images AS ProfileImage WHERE id = $id ORDER BY id DESC";
    $result = $this->query($sql);
    return $result;
  }

}

?>
