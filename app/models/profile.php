<?php

class Profile extends Model
{
  var $name = "Profile";
  
  var $hasMany = array('ProfileImage' =>
                         array('className'     => 'ProfileImage',
                               'conditions'    => '',
                               'order'         => '',
                               'limit'         => '',
                               'foreignKey'    => '',
                               'dependent'     => true,
                               'exclusive'     => false,
                               'finderQuery'   => 'SELECT * FROM profile_images WHERE Profile.user_id=Profile_Image.user_id'
                         )
                      );
  
                      	
	var $validate = array
	(
		'firstname' => VALID_NOT_EMPTY,
		'lastname' => VALID_NOT_EMPTY,
		'city' => VALID_NOT_EMPTY,
		'state' => VALID_NOT_EMPTY,
		'title' => VALID_NOT_EMPTY,
		'introduction' => VALID_NOT_EMPTY,
		'description' => VALID_NOT_EMPTY,
		'availability' => VALID_NOT_EMPTY,
		'experience' => VALID_NOT_EMPTY,
		'cuisines' => VALID_NOT_EMPTY,
		'specialities' => VALID_NOT_EMPTY,
		'influences' => VALID_NOT_EMPTY
	); 
  
	function beforeSave()
	{
		return true;
	}
  

  function getAll()
  {  
     $result = $this->query("SELECT * FROM profiles AS Profile WHERE availability = '1' ORDER BY id DESC");
  
     if (isset($result))
        return $result;
     else
        return null;
  }

  function getAllByState()
  {  
     $result = $this->query("SELECT * FROM profiles AS Profile WHERE availability = '1' ORDER BY state, firstname, lastname ");
  
     if (isset($result))
        return $result;
     else
        return null;
  }

  function getProfileById($id=null)
  {
     $result = $this->query("SELECT * FROM profiles AS Profile WHERE id = $id");
     if (isset($result))
        return $result[0];
     else
        return null;
  }

  function getProfileByUserId($id=null)
  {
     $result = $this->query("SELECT * FROM profiles AS Profile WHERE user_id = $id");
     if (isset($result))
        return $result[0];
     else
        return null;
  }

  function getProfilesByStates($state=null)
  {
     $result = $this->query("SELECT state,COUNT(*) as 'count' FROM profiles GROUP BY state");
     if (isset($result))
        return $result;
     else
        return null;
  }

	function getAvailabilityLevels()
	{
								
		$availability = array(
								1=>"Yes",
								0=>"No");
		
		return $availability;

	}

	function getMessageOptions()
	{
								
		$options = array(
								"Y"=>"Yes",
								"N"=>"No");
		
		return $options;

	}
	
	function getExperienceLevels()
	{
		$experience = array(
								1=>"Student",		            
								6=>"6 Months",
								12=>"1 Year",
								24=>"2 Years",
								36=>"3 Years",
								48=>"4 Years",
								60=>"5 Years",
								72=>"6 Years",
								84=>"7 Years",
								96=>"8 Years",
								108=>"9 Years",
								120=>"10 Years +"
							);
		return $experience;
	}
}

?>
