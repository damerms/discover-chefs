<?php

class User extends AppModel
{

  var $name = "User";
  
  //
  //  Validation Rules
  //


 	var $validate = array
		(
				'email' => array(
					'validemail'=>array('rule'=>array('email'),'message'=>'Email address is not valid'),
					'isunique' => array('rule'=>array('isUniqueEmail'),'message'=>'This email address has already been registered')),
				'password' => array('rule'=>array('minLength',4),'message'=>'Password must be at least 6 charcters'),
				'confirm' => array('rule'=>array('passwordCompare'),'message'=>'Confirmation does not match password'),
				'sitename' => array(
					'required'=>array('rule'=>array('minLength',1),'message'=>'Your personal domain name must be entered.(i.e. johnsmith = johnsmith.discoverchefs.com)'),
					'unique'=>array('rule'=>array('isUniqueSitename'),'message'=>'This domain name is already in use.')),
				'agreement' => array('rule'=>array('comparison','>',0),'message'=>'Agreement must be checked.'),
   		); 


	//
	//	Validation - Custom Methods
	//
	
	//	Check Email Has Not Been Registered Already
	
	function isUniqueEmail()
	{
		$result = $this->query("SELECT email FROM users WHERE email = '".$this->data['User']['email']."'");
		if (!empty($result) && empty($this->data["User"]["id"]))
		{
			return false;
		}	
		else
		{
			return true;
		}
	}

	//	Check Password and Confirmation Match
	
	function passwordCompare()
	{
		if ($this->data['User']['password'] == $this->data['User']['confirm'])
		{
			return true;	
		}
		else
		{
			return false;			
		}
	}

	//	Check Personal Domain Is Not In Use
	
	function isUniqueSiteName()
	{
		$result = $this->query("SELECT sitename FROM users WHERE sitename = '".$this->data['User']['sitename']."'");
		if (!empty($result) && empty($this->data["User"]["id"]))
		{
			return false;
		}	
		else
		{
			return true;
		}
		
		return true;
	}

  //
  //  Data Methods
  //
  //

  function getBySiteName($sitename)
  {
    $result = $this->query("SELECT * FROM users AS User WHERE sitename = '".$sitename."'");

    if (!empty($result))
      return $result[0];
    else
      return null;
  }
  
	//
  //  Athenticate User
  //

  function authenticateUser($email)
  {
      $user = $this->findByEmail($email);
      if (empty($user))
        return "Error: This email address was not found";
      else
        return "";
  }


  function authenticatePassword($email,$password)
  {
      $user = $this->findByEmail($email);
      if ($user["User"]["password"] != $password)
        return "Error: This password is not valid for this email address";
      else
        return "";
  }

  function isValidUser($username,$password)
  {
      return true;
  }

  //
  //  Check If User Has Created A Profile Record
  //

  function checkProfileExists($userid)
  {
    $sql = "SELECT id FROM profiles WHERE user_id = $userid";
    $result = $this->query($sql);

    if ($result)
      $id = $result[0]["profiles"]["id"];

    if (!empty($id ))
      return true;
    else
      return false;
  }

}
?>
